package com.example;

public class Account {
    private static float balance;

    public static float getBalance() {
        return balance;
    }

    public static void setBalance(float balance) {
        Account.balance = balance;
    }

    public void debit(float amount){
        balance=balance-amount;
    }
    public static void  deposit(float amount){
        balance=balance+amount;
    }

}
