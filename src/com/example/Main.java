package com.example;

public class Main {
    public static void main(String[] args) {
        Account account = new Account();
        Account.setBalance(10000);
        account.debit(5000);
        System.out.println(Account.getBalance());

        Account account2 = new Account();
        //account2.debit(5000);
        Account.deposit(1000);
        System.out.println(Account.getBalance());
    }
}
